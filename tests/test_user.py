from typer.testing import CliRunner

from celestical import api
from celestical.cli import app
from celestical.configuration import API_URL
from celestical.user import (
    login_form,
    load_user_creds
    )


def test_login_form():
    runner = CliRunner()
    res = runner.invoke(app, ["login"],
          input="n\n\n")

    assert res.exit_code == 1
    assert "Email is incorrect" in res.stdout


def test_load_creds():
    preauth = "zero_token"
    apiconf = api.Configuration(host=API_URL)
    apiconf.api_key['Authorization'] = preauth
    res, msg = load_user_creds(apiconf)

    assert isinstance(res, bool)
    assert isinstance(msg, str)
    assert res == False or (res == True and
            apiconf.api_key['Authorization'] != preauth)


# def test_login_form2():
#     runner = CliRunner()
#     res = runner.invoke(app, ["login"],
#           input="n\n\ngood@emAi000zzz0dhgqygfeuho.com\npassw000rd\npassw000rd\n")
# 
#     assert res.exit_code == 0
#     assert "Sorry user/password are not matching" in res.stdout
#     assert "Not logged in" in res.stdout

from celestical.docker_local import (
    get_image_hash,
    get_ports,
    get_docker_client,
    compress_image
    )

def test_image_hash():
    """ Testing nginx:1.24 is here """
    nginx_hash = get_image_hash("nginx:1.24")
    assert "6c0218f16876" in nginx_hash


def test_image_ports():
    nginx_hash = get_image_hash("nginx:1.24")
    ports = get_ports(nginx_hash)
    assert "80" in ports


def test_docker_client():
    dc = get_docker_client()
    assert dc is not None, "Make sure docker service is running"


def test_images_preparation():
    output = compress_image("ImaGE--D-O--NOTEXIST:98.2", "projectA")
    assert len(output) == 0

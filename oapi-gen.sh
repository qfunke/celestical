#npx @openapitools/openapi-generator-cli generate -i openapi.json --http-user-agent "Celestical Python Client"  -g python -o openapi-generator/ --package-name celestical.api

if [ ! -f ./pyproject.toml ]; then
	echo "cannot proceed: Please call that script from the repo root where pyproject.toml file is."
	exit 2
fi

if [ ! -f ./openapi.json ]; then
	echo "./openapi.json file not found."
	exit 3
fi

echo " ---------- Generating API client code from local openapi.json file."
CLIVERSION=$(grep version ./pyproject.toml | head -n1 | sed -e "s/.* \"//" -e 's/\".*//')
npx @openapitools/openapi-generator-cli generate -i openapi.json --http-user-agent "celestical/py/${CLIVERSION}"  -g python -o openapi-generator/ --package-name celestical.api

if [ ! $? -eq 0 ]; then
	echo " ---------- exiting with generating issues."
	echo "            current setup is untouched."
	exit 4
fi

echo " ---------- Saving past api generation."
if [ -d ./celestical--api--.old.1 ]; then 
	if [ -d ./celestical--api--.old.2 ]; then 
		rm -rf ./celestical--api--.old.1
		rm -rf ./celestical--api--.old.2
		mv src/celestical/api celestical--api--.old.1
	else
		mv src/celestical/api celestical--api--.old.2
	fi
else
	mv src/celestical/api celestical--api--.old.1
fi

echo " ---------- Copying generated API client scripts."
cp -r openapi-generator/celestical/api src/celestical/api

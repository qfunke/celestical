import pytest
from unittest.mock import patch, MagicMock

import docker
from prettytable import PrettyTable
from typer.testing import CliRunner
from typer import Typer

import celestical.cli as cli 


def test_docker_connection():
    """
    Test the connection to the Docker engine.
    
    This function creates a Docker client and attempts to ping the Docker engine.
    If the ping is successful, the test passes. If the ping fails, the test fails
    with a message that includes the error from the Docker engine.
    """
    client = docker.from_env()
    try:
        client.ping()
    except docker.errors.APIError as e:
        pytest.fail(f"Failed to connect to Docker engine: {e}")
    finally:
        client.close()


def test_list_local_images():
    """
    Test the images listing function.
    
    This function calls list_local_images which is called in images() and checks
    if the returned value is an instance of PrettyTable. It also checks if the
    table has the correct field names.
    """
    table = cli.list_local_images()
    assert isinstance(table, PrettyTable), "Returned table is not a PrettyTable"
    assert table.field_names == ["Image ID", "Image Name", "Tags", "Ports"], "Incorrect field names in the table"


def test_list_images():
    runner = CliRunner()
    result = runner.invoke(cli.app, ["images"])
    assert result.exit_code == 0
    assert "The following are your local docker images" in result.output


def test_help_welcome():
    """ Test welcome message has minimum viable infos

        Drawing of lines are not regular pipes:
    │ apps
    """
    runner = CliRunner()
    for cmd in [[], ["--help"]]:
        result = runner.invoke(cli.app, cmd)
        assert result.exit_code == 0
        assert "Usual workflow steps" in result.output
        assert "Usual workflow steps" in result.stdout
        for info in ["apps", "deploy", "login", "register"]:
            assert "│ "+info in result.stdout
        assert "[/" not in result.stdout


# To rewrite with new client version: DEPLOY function

#@patch('celestical.cli.docker.from_env')
#@patch('celestical.cli.typer.prompt')
#@patch('celestical.cli.typer.confirm')
#def test_deploy(mock_confirm, mock_prompt, mock_docker):
#    # Set return values for the mock objects
#    mock_prompt.side_effect = ['image_name', 'tag', 'context', 'domain']
#    mock_confirm.return_value = True
#    mock_docker.return_value = MagicMock()
#
#    # Call the upload function
#    cli.deploy()
#
#    # Assert that the mock objects were called with the expected arguments
#    mock_prompt.assert_any_call("\nWhich image would you like to upload?")
#    mock_prompt.assert_any_call("Is there a tag you prefer? (default:latest)")
#    mock_prompt.assert_any_call("What is the context of the app/image?")
#    mock_prompt.assert_any_call("What is the domain to reach this app/image?")
#    mock_confirm.assert_called_once_with("Would you like to proceed with saving and compressing the image?")
#    assert mock_docker.called

import pytest
import typer
import json
from pathlib import Path

from celestical.compose import (
    richformat_services,
    read_docker_compose,
    check_for_enrichment,
    load_dot_env,
    integrate_all_env,
    define_compose_path
    )
# To redefine c.helper.confirm_user()
import celestical

celestical.configuration.BATCH_MODE = True

@pytest.fixture
def stack_info():
    stack = {
        "version": "3.7",
        "services": {
            "db": {
                "celestical_type": "DB",
                "image": "mariadb:latest",
                "ports": "90:90",
                "environment": ["user=ATILA","mdp=${VAR2}"]
            },
            "web": {
                "image": "adminer:latest",
                "ports": "9019:8080",
                "env_files": ["/tmp/.celestical.envfilestest"]
            }
        },
        "networks": {},
        "celestical": {
            "app_id": "AI4CE",
            "base_domain": "ai4ce.space"
        }
    }

    envfile = Path(stack["services"]["web"]["env_files"][0])
    # reset file just in case
    envfile.touch()
    envfile.unlink()
    envfile.touch()

    with envfile.open(mode="w") as fff:
        fff.write("EF1=523.3\n")
        fff.write("EF2=StarTrek-Cloud\n")
        fff.write("EF3=https://parametry.ai/os")

    return stack


def test_richformat_services(stack_info):
    """ Testing if richformatting of images works """
    fstr = richformat_services(stack_info["services"])
    assert fstr == \
        "\t- [yellow]db[/yellow] (image)--> " \
        + "mariadb:latest\n" \
        + "\t- [yellow]web[/yellow] (image)--> " \
        + "adminer:latest\n"


def test_define_compose_path_error():
    """ Testing wrong inputs or loading compose files """
    F1, F2 = define_compose_path("/tmpbloblabli/")
    assert F1 is None
    assert F2 is None


def test_define_compose_path_ok():
    """ Testing wrong inputs or loading compose files """
    tmpdir = Path("/tmp")
    dcfile = tmpdir / "docker-compose.yml"
    dcfile.touch()
    edcfile = tmpdir / ".docker-compose-enriched.yml"

    F1, F2 = define_compose_path(dcfile)
    assert F1 == dcfile
    assert F2 == edcfile

    assert F1.is_file()
    assert not F2.is_file()

    dcfile.unlink()
    assert not F1.is_file()


def test_define_compose_path_ok_compose():
    """ Testing wrong inputs or loading compose files """
    tmpdir = Path("/tmp")
    dcfile = tmpdir / "docker-compose.yml"
    cfile = tmpdir / "compose.yml"
    edcfile = tmpdir / ".docker-compose-enriched.yml"

    F1, F2 = define_compose_path(tmpdir)
    assert F1 != dcfile
    assert F1 == cfile
    assert F2 == edcfile

    assert not F1.is_file()
    assert not F2.is_file()


def test_read_docker_compose():
    """ Testing wrong inputs or loading compose files """
    comp = read_docker_compose("/tmp/")
    assert comp == {}


def test_check_for_enrichment_abort():
    """ Check for enrichment and see if it aborts for the right reasons
    typically when the path of compose file could not be determined.

        for non existing folder
    """
    tmpdir = Path("/tmpblablielbi")
    with pytest.raises(typer.Abort) as e_info:
        latestfile, comp, ecomp = check_for_enrichment(tmpdir)


def test_check_for_enrichment_abort1():
    """ Check for enrichment 
        with non existing files in an existing folder
    """
    # Hack for predefined user inputs
    celestical.helper.confirm_user = lambda: True
    celestical.configuration.BATCH_MODE = True

    tmpdir = Path("/tmp")
    with pytest.raises(typer.Abort) as e_info:
        check_for_enrichment(tmpdir)
    with pytest.raises(typer.Abort) as e_info:
        check_for_enrichment("/tmp/")
    with pytest.raises(typer.Abort) as e_info:
        check_for_enrichment("/tmp")


def test_check_for_enrichment_empty_compose():
    """ Check for enrichment 
        with folder as input containing an existing file
    """
    # Hack for predefined user inputs
    celestical.helper.confirm_user = lambda x,y: True
    celestical.configuration.BATCH_MODE = True

    tmpdir = Path("/tmp")
    dcfile = tmpdir / "docker-compose.yml"
    dcfile.touch()
    latestfile, comp, ecomp = check_for_enrichment("/tmp")

    assert isinstance(latestfile, Path)
    assert isinstance(comp, dict)
    assert isinstance(ecomp, dict)
    dcfile.unlink()


def test_check_for_enrichment_ok(stack_info):
    """ Check for enrichment 
        with folder as input containing an existing file
    """
    # Hack for predefined user inputs
    celestical.helper.confirm_user = lambda x,y : True
    celestical.configuration.BATCH_MODE = True

    tmpdir = Path("/tmp")
    dcfile = tmpdir / "docker-compose.yml"
    with dcfile.open(mode="w") as fff:
        json.dump(stack_info, fff)
    latestfile, comp, ecomp = check_for_enrichment("/tmp")

    assert isinstance(latestfile, Path)
    assert isinstance(comp, dict)
    assert isinstance(ecomp, dict)

    dcfile.unlink()


def test_load_dot_env_empty():
    """ Check for .env file content when empty or not here
    """
    tmpdir = Path("/tmp")
    dotfile = tmpdir / ".env"
    dotfile.touch()
    dotfile.unlink()

    # no file 
    env = load_dot_env(tmpdir)
    assert isinstance(env, dict)
    assert len(env) == 0

    # empty file 
    dotfile.touch()
    env = load_dot_env(tmpdir)
    assert len(env) == 0
    assert env == {}

    dotfile.unlink()


def test_load_dot_env_notempty():
    """ Check for .env file content when it is not empty
    """
    tmpdir = Path("/tmp")
    dotfile = tmpdir / ".env"
    dotfile.touch()

    with dotfile.open(mode="w") as fff:
        fff.write("VAR1='Great Wo Mantorships'\n")
        fff.write("var_two=Zhong Guo Hen Da")

    env = load_dot_env(tmpdir)

    assert len(env) == 2
    assert "VAR1" in env
    assert "var_two" in env
    assert env["VAR1"] == "'Great Wo Mantorships'"
    assert env["var_two"] == "Zhong Guo Hen Da"
    dotfile.unlink()


def test_integrate_env(stack_info):
    """ Check for .env file content when it is not empty
    """
    tmpdir = Path("/tmp")
    dotfile = tmpdir / ".env"
    dotfile.touch()

    with dotfile.open(mode="w") as fff:
        fff.write("VAR1='Great Wo Mantorships'\n")
        fff.write("VAR2=MikeJaggerInSpace")

    comp = integrate_all_env(stack_info, tmpdir)

    assert len(comp) == 4
    assert "services" in comp
    assert "db" in comp["services"] 
    assert "environment" in comp["services"]["db"]

    env = comp["services"]["db"]["environment"]
    assert env["user"] == "ATILA"
    assert env["mdp"] == "MikeJaggerInSpace"

    dotfile.unlink()


def test_integrate_envfiles(stack_info):
    """ Check for .env file content when it is not empty
    """
    comp = integrate_all_env(stack_info, Path("/tmp"))

    # testing loading of file in env_files
    webenv = comp["services"]["web"]["environment"]
    for var in ["EF1", "EF2", "EF3"]:
        assert var in webenv
    assert webenv["EF1"] == "523.3"
    assert webenv["EF2"] == "StarTrek-Cloud"
    assert webenv["EF3"] == "https://parametry.ai/os"


# Celestical Cloud CLI

The Independent Green European Serverless Cloud made by Space Engineers, powered by Parametry.ai

This repo is the code for the command line client. It is also your best
companion to include in a batch continuous deployment process.

Our rationale is that we can extract all necessary information from your local
`docker-compose.yml` file to be able to deploy your application.

So...
 - You do your docker-compose
 - We do the rest.


## Quick start

``` bash
pip install celestical
celestical
```

You get the help for the celestical command line.
The following command will deploy your app if you have an account 

``` bash
celestical deploy
```


## For Developers and Contributors

This client is open source, you have the freedom to adapt it to your own needs
as well as to [create issues on things that you don't like or don't work
perfectly](https://gitlab.com/parametry-ai/celestical/pws_cli/-/issues/new)

### Roadmap
 - [x] Creating a functional enriched docker-compose file
 - [x] Pushing all necessary information to Celestical
 - [ ] Ability to call in batch mode (for CI/CD purposes)
 - [ ] Viewing deployment status of all my applications
 - [ ] Deploying to space


### Using poetry

You can create a poetry env with `poetry install` and load it with `poetry shell`.

More in [OPENAPI.md](OPENAPI.md) about how to generate API client code.


import pytest
import typer
import json
from pathlib import Path

from celestical.compose import (
    load_dot_env,
    integrate_all_env,
    _apply_variables,
    )

import celestical
celestical.configuration.BATCH_MODE = True

@pytest.fixture
def stack_info():
    stack = {
        "version": "3.7",
        "services": {
            "db": {
                "celestical_type": "DB",
                "image": "mariadb:latest",
                "ports": "90:90",
                "environment": ["user=ATILA","mdp=${VAR2}"]
            },
            "db2": {
                "celestical_type": "DB",
                "image": "mariadb:latest",
                "ports": "90:90",
                "environment": {"user": 'ATILA',"mdp": '${VAR2}'}
            },
            "web": {
                "image": "adminer:latest",
                "ports": "9019:8080",
                "env_files": ["/tmp/.celestical.envfilestest"]
            }
        },
        "networks": {},
        "celestical": {
            "app_id": "AI4CE",
            "base_domain": "ai4ce.space"
        }
    }

    envfile = Path(stack["services"]["web"]["env_files"][0])
    # reset file just in case
    envfile.touch()
    envfile.unlink()
    envfile.touch()
    with envfile.open(mode="w") as fff:
        fff.write("EF1=523.3\n")
        fff.write("EF2=StarTrek-Cloud\n")
        fff.write("EF3=https://parametry.ai/os")
    return stack


def test_integrate_envfiles(stack_info):
    """ Check for .env file content when it is not empty
    """
    tmpdir = Path("/tmp")
    dotfile = tmpdir / ".env"
    dotfile.touch()
    dotfile.unlink()

    assert len(stack_info["services"]) == 3
    comp = integrate_all_env(stack_info, Path("/tmp"))

    # counter test when no .env file is present
    env = comp["services"]["db"]["environment"]

    assert isinstance(env, dict)
    assert len(env) == 2
    assert env["user"] == "ATILA"
    assert env["mdp"] == "${VAR2}"
    # this asserts "${VAR2}" not in .env

    # testing loading of file in env_files
    webenv = comp["services"]["web"]["environment"]
    for var in ["EF1", "EF2", "EF3"]:
        assert var in webenv
    assert webenv["EF1"] == "523.3"
    assert webenv["EF2"] == "StarTrek-Cloud"
    assert webenv["EF3"] == "https://parametry.ai/os"


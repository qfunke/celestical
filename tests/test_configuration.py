
from celestical.configuration import (
    welcome
    )

def test_welcome():
    welcome_msg = welcome()

    assert isinstance(welcome_msg, str)
    assert "[/" in welcome_msg
    assert "{" not in welcome_msg
    assert "}" not in welcome_msg

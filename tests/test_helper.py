import json
import os
import yaml

import pytest

from celestical.helper import (
    save_json,
    save_yaml,
    extract_all_dollars,
    dict_to_list_env)


@pytest.fixture
def stack_info():
    return {
        "compose": {
            "version": "3.7",
            "services": {
                "db": {
                    "image": "mariadb:latest",
                    "ports": "90:90"
                },
                "web": {
                    "image": "adminer:latest",
                    "ports": "19:19"
                }
            },
            "networks": {}
        },
        "name": "AI4CE"
    }


def test_save_stack_json(tmpdir, stack_info):
    """Test the save_json function.
    Params:
        tmpdir: pytest fixture to create a temporary directory
        stack_info(dict): complete info about the stack (name, compose ..)
    Returns:
    """
    # Use pytest's temporary directory feature
    with tmpdir.as_cwd():
        # Call the function to save the JSON file
        save_json(stack_info)

        # Check if the file was created
        json_file = f'stack_{stack_info["name"]}.json'
        assert os.path.exists(json_file), "JSON file was not created"

        # Check if the file content is correct
        with open(json_file, 'r') as file:
            data = json.load(file)
            assert data == stack_info, "JSON file content is incorrect"


def test_save_compose(tmpdir, stack_info):
    """Test the save_yaml function.
    Params:
        tmpdir: pytest fixture to create a temporary directory
        stack_info(dict): complete info about the stack (name, compose ..)
    Returns:
    """
    # Use pytest's temporary directory feature
    with tmpdir.as_cwd():
        # Call the function to save the YAML file
        # it should be saved as docker-compose-enriched.yml 
        save_yaml(stack_info["compose"])

        # Check if the file was created
        yml_file = '.docker-compose-enriched.yml'
        assert os.path.exists(yml_file), "YAML file was not created"

        # Check if the file content is correct
        with open(yml_file, 'r') as file:
            data = yaml.safe_load(file)
            assert data == stack_info["compose"], "YAML file content is incorrect"


def test_extract_dollars_no_dollars():
    """Test the extract_all_dollars function

        This function generate a dictionary of
        variable names and how they are referred in the string
        like $this or like ${that}

    """

    test1 = "This string? It has no variables!"
    test2 = "That string has no dollars even if $ is here."

    res1 = extract_all_dollars(test1) 
    assert len(res1) == 0
    res2 = extract_all_dollars(test2) 
    assert len(res2) == 0
    

def test_extract_dollars_curly():
    """Test the extract_all_dollars function

        This function generate a dictionary of
        variable names and how they are referred in the string
        like $this or like ${that}

    """

    test1 = "${VAR4} in a string"
    test2 = "Not beginning $GOO${VAR4}"

    res1 = extract_all_dollars(test1) 
    assert len(res1) == 1
    assert "VAR4" in res1
    assert res1["VAR4"] == "${VAR4}"

    res2 = extract_all_dollars(test2) 
    assert len(res2) == 2
    assert res2["VAR4"] == "${VAR4}"
    assert res2["GOO"] == "$GOO"


def test_extract_dollars():
    """Test the extract_all_dollars function

        This function generate a dictionary of
        variable names and how they are referred in the string
        like $this or like ${that}

    """

    test0 = "$VAR4"
    test0_2 = "$GOO$VAR4"
    test1 = "This string? It has a $variable!"
    test2 = "That $string has $dollars even if it is just one."

    res0 = extract_all_dollars(test0) 
    assert len(res0) == 1
    assert "VAR4" in res0
    assert res0["VAR4"] == "$VAR4"

    res0_2 = extract_all_dollars(test0_2) 
    assert len(res0_2) == 2
    assert res0_2["VAR4"] == "$VAR4"
    assert res0_2["GOO"] == "$GOO"

    res1 = extract_all_dollars(test1) 
    assert len(res1) == 1
    assert "variable" in res1
    assert res1["variable"] == "$variable"

    res2 = extract_all_dollars(test2) 
    assert len(res2) == 2
    assert res2["string"] == "$string"
    assert res2["dollars"] == "$dollars"
    

def test_dict_to_list_empty():
    """ Important in translating environment saved as dict into list

        from key: value
        to key=value
    """
    envdict = {
    }

    envlist = dict_to_list_env(envdict)
    assert envlist == []


def test_dict_to_list():
    """ Important in translating environment saved as dict into list

        from key: value
        to key=value
    """
    envdict = {
        "var0": "This is a beautiful night",
        "var1": "234",
        "var2": ""
    }

    envlist = dict_to_list_env(envdict)
    assert len(envlist) == 3
    assert envlist[0] == "var0=This is a beautiful night"
    assert envlist[1] == "var1=234"
    assert envlist[2] == "var2="
